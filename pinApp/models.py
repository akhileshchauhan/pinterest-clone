from django.db import models
from app.models import customUser

class Pin(models.Model):
    user = models.ForeignKey(customUser, on_delete=models.CASCADE)
    pin_images = models.ImageField(upload_to='pin_images/')
    title = models.CharField( max_length=200)
    description = models.CharField( max_length=100)
    link = models.URLField()
    
    
class Follow(models.Model):
    follower = models.ForeignKey(customUser, related_name='following', on_delete=models.CASCADE)
    followed = models.ForeignKey(customUser, related_name='followers', on_delete=models.CASCADE)
    
    class Meta:
        unique_together = ['follower', 'followed']
        
        
        
class Comment(models.Model):
    text = models.TextField()
    author = models.ForeignKey(customUser, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    parent_comment = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='replies')
    pin = models.ForeignKey(Pin, on_delete=models.CASCADE, related_name='comments')

    def __str__(self):
        return f'Comment by {self.author.username}'

    class Meta:
        ordering = ['created_at']

    def get_replies(self):
        """Method to retrieve all replies to this comment."""
        return Comment.objects.filter(parent_comment=self)
    
    
    
    

