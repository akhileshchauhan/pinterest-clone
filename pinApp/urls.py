from django.urls import path
from . import views
urlpatterns = [
    path('upload_pin',views.upload_pin,name='upload_pin'),
    path('pin_list/<int:user_id>/',views.pin_list,name='pin_list'),
    path('comment/<int:pin_id>/', views.comment_pin, name='comment_pin'),
    path('pin/<int:image_id>/', views.pin, name='image_detail'),
    path('pin/<int:pin_id>/delete/', views.delete_pin, name='delete_pin'),
    path('edit_pin/<int:pin_id>/', views.edit_pin, name='edit_pin'),
    path('follow/<str:username>/', views.follow_user, name='follow_user'),
    path('followers/<str:username>/', views.followers_list, name='followers_user'),
    path('following/<str:username>/', views.following_list, name='following_user'), 
    path('comment_data/<int:image_id>/', views.comments_detail, name='comment_data'),
     path('pin/<int:pin_id>/comment/<int:parent_id>/', views.comment_reply, name='comment_reply'),
     
    #
  

    # path('followers_count/', views.follwers_count, name='followers_count'), 
]