from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
from app.models import customUser
from .models import Pin,Follow,Comment
from django.http import JsonResponse
from django.core.serializers import serialize


# from .models import Pin


@login_required(login_url='/login/')
def upload_pin(request):
    if request.method == 'POST':
        pin_images = request.FILES['pin_images']
        title = request.POST['title']
        description = request.POST['description']
        link = request.POST['link']
        user = request.user
        Pin.objects.create(user=user, pin_images=pin_images,
                           title=title, description=description, link=link)
        user_id = request.user.id
        return redirect('pin_list',user_id)
    return render(request, 'pin/index.html')


@login_required(login_url='/login/')
def pin_list(request,user_id):
    pins = Pin.objects.filter(user_id=user_id)
    return render(request, 'pin/pin_list.html', {'pins': pins})
  


# @login_required(login_url='/login/')
def pin(request, image_id):
    image = get_object_or_404(Pin, pk=image_id)
    username = list(customUser.objects.filter(id=image.user_id).values())
    follower_count = Follow.objects.filter(followed=image.user_id).count()
    print(username)
    return render(request, 'pin/pin.html', {'pins': image,'user':username,'follower_count':follower_count})


@login_required(login_url='/login/')
def delete_pin(request, pin_id):
    pinned_image = get_object_or_404(Pin, pk=pin_id)
    
    if pinned_image.user != request.user:
        return HttpResponseForbidden("You do not have permission to delete this pin.")

    if request.method == 'POST':
        pinned_image.pin_images.delete()
        pinned_image.delete()
        return redirect('pin_list',request.user.id)

    return render(request, 'pin/pin_detail.html', {'pins': pinned_image})


@login_required(login_url='/login/')
def edit_pin(request, pin_id):
    pin = get_object_or_404(Pin, id=pin_id)
    
    if pin.user != request.user:
        # If not, return a 403 Forbidden response
        return HttpResponseForbidden("You do not have permission to edit this pin.")

    
    if request.method == 'POST':
        pin.title = request.POST.get('title')
        pin.description = request.POST.get('description')
        pin.save()
        return redirect('image_detail', pin.id) 
        
    return render(request, 'pin/edit_pin.html', {'pin': pin})



@login_required
def follow_user(request, username):
    user_to_follow = get_object_or_404(customUser, username=username)

    print(request.method)
    if request.method == 'POST':
        if request.user == user_to_follow:
            return JsonResponse({'error': 'You cannot follow yourself.'}, status=400)

        if Follow.objects.filter(follower=request.user, followed=user_to_follow).exists():
            return JsonResponse({'error': 'You are already following this user.'}, status=400)
        
        Follow.objects.create(follower=request.user, followed=user_to_follow)
       
        return JsonResponse({'success': 'You are now following {}.'.format(username)})
        
    elif request.method == 'DELETE':    
        
        follow_instance = Follow.objects.filter(follower=request.user, followed=user_to_follow)
        if not follow_instance.exists():
            return JsonResponse({'error': 'You are not following {}.'.format(username)}, status=400)

        
        follow_instance.delete()
        return JsonResponse({'success': 'You have unfollowed {}.'.format(username)})

    return JsonResponse({'error': 'Invalid request method.'}, status=405)




def followers_list(request, username):
 
    user = get_object_or_404(customUser, username=username)
    followers = Follow.objects.filter(followed=user)
    follower_usernames = [follower.follower.username for follower in followers]
    return JsonResponse({'followers': follower_usernames})

def following_list(request, username):
    user = get_object_or_404(customUser, username=username)
    following = Follow.objects.filter(follower=user)
    following_usernames = [followed.followed.username for followed in following]
    return JsonResponse({'following': following_usernames})


# @login_required
# def follwers_count(request):
#     user = request.user
#     follower_count = Follow.objects.filter(followed=user.id).count()
#     print('abc')
#     return render(request, 'pin/pin.html', {'follower_count': follower_count})



@login_required(login_url='/login/')
def comment_pin(request, pin_id):
    pin = Pin.objects.get(pk=pin_id)
    comments = Comment.objects.filter(pin=pin, parent_comment__isnull=True).select_related('author').prefetch_related('replies__author')

    if request.method == 'POST':
        text = request.POST.get('comment-text')
        parent_id = request.POST.get('parent_id')
        if text:
            author = request.user
            parent_comment = Comment.objects.get(pk=parent_id) if parent_id else None
            new_comment = Comment.objects.create(text=text, author=author, pin=pin, parent_comment=parent_comment)
            return redirect('image_detail', image_id=pin_id)

    return render(request, 'pin/comments.html', {'pin': pin, 'comments': comments})




@login_required(login_url='/login/')
def comment_reply(request, pin_id, parent_id):
    pin = get_object_or_404(Pin, pk=pin_id)
    parent_comment = get_object_or_404(Comment, pk=parent_id)
    
    if request.method == 'POST':
        text = request.POST.get('comment-text')
        if text:
            author = request.user
            Comment.objects.create(text=text, author=author, pin=pin, parent_comment=parent_comment)
            return redirect('comment_pin', pin_id=pin_id)
    
    return redirect('comment_pin', pin_id=pin_id)


def comments_detail(request, image_id):
    pin = Pin.objects.get(id=image_id)
    comments = Comment.objects.filter(pin=pin)
    comments_json = serialize('json', comments)
    return JsonResponse(comments_json, safe=False)


