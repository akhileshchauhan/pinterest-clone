from django.db import models

# Create your models here.


from django.contrib.auth.models import AbstractUser

class customUser(AbstractUser):
    # groups = models.ManyToManyField('auth.Group', related_name='customuser_set', blank=True)
    # user_permissions = models.ManyToManyField('auth.Permission', related_name='customuser_set', blank=True)
    
    username = models.CharField(unique=True, max_length=20)
    email = models.EmailField(unique=True)
    password = models.CharField(unique=True, max_length=10)
 

    def str(self):
        return self.username
