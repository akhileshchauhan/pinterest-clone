from django.contrib.auth.forms import UserCreationForm,AuthenticationForm

from django.contrib.auth.models import User

from django import forms

from django.forms.widgets import PasswordInput, TextInput

class CreateUserForm(UserCreationForm):
    username = forms.CharField(widget=TextInput(attrs={'placeholder': 'Enter your username'}))
    email = forms.EmailField(widget=TextInput(attrs={'placeholder': 'Enter your email'}))
    password1 = forms.CharField(widget=PasswordInput(attrs={'placeholder': 'Enter your password'}))
    password2 = forms.CharField(widget=PasswordInput(attrs={'placeholder': 'Confirm your password'}))
    
    class Meta:
        
        model = User
        fields = ['username','email','password1','password2']
        

      
class LoginForm(AuthenticationForm):
    
    username = forms.CharField(widget=TextInput())
    password = forms.CharField(widget=PasswordInput())
    

        
