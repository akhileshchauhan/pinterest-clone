from django.urls import path
from . import views
urlpatterns = [
    path('',views.homepage,name=''),
    path('register/',views.register,name='register'),
    path('registration/',views.registration,name='registration'),
    path('my-login/',views.my_login,name='my-login'),
    path('login/',views.login_view,name='login'),
    path('dashboard/<str:username>/',views.dashboard,name='dashboard'),
    path('user-logout/',views.user_logout,name='user-logout'),
    path('user-page/',views.user_page,name='user-page'),
]