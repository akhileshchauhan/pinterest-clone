from django.shortcuts import render, redirect
from .models import customUser
from pinApp.models import Pin,Follow
# Create your views here.
from . forms import CreateUserForm, LoginForm


# from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import auth
from django.contrib.auth import authenticate, login, logout
from django.core.serializers import serialize


def homepage(request):
    
    pins = Pin.objects.all()
    
    return render(request, 'template-app/index.html',{'pins':pins})


def register(request):
    form = CreateUserForm()

    if request.method == "POST":
        form = CreateUserForm(request.POST)

        if form.is_valid():

            form.save()

            return redirect('my-login')

    context = {'registerform': form}

    return render(request, 'template-app/register.html', context=context)


def my_login(request):

    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(request, data=request.POST)

        if form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                auth.login(request, user)

                return redirect('dashboard')

    context = {'loginform': form}

    return render(request, 'template-app/my-login.html', context=context)


def user_logout(request):
    auth.logout(request)
    return redirect("")


# @login_required(login_url='login')
# def dashboard(request):
#     return render(request, 'template-app/dashboard.html')


def registration(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        print(password)
        if customUser.objects.filter(username=username).exists() or customUser.objects.filter(email=email).exists():
            return render(request, 'template-app/registration.html', {'error_message': "Username or email already exists"})

        # Create the user
        user = customUser.objects.create(
            username=username, email=email)
        user.set_password(password)
        user.save()
        # Log in the user
        login(request, user)

        return redirect('login')  # Redirect to a success page

    return render(request, 'template-app/registration.html')


def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        # print( customUser.objects.filter(username = username))
        user = authenticate(username=username, password=password)
        print(user)
        if user is not None:
            login(request, user)
            # Redirect to a success page
            return redirect('/user-page')
        else:
            return render(request, 'template-app/login.html', {'error_message': "Invalid username or password"})
    else:
        return render(request, 'template-app/login.html')


def dashboard(request,username):
    profile = customUser.objects.filter(username=username).values()
    user= customUser.objects.get(username=username)
    user_id = user.id
    print(profile)
    follower_count = Follow.objects.filter(followed=user_id).count()
    following_count = Follow.objects.filter(follower=user_id).count()
    return render(request, 'template-app/dashboard.html', {'profile': profile,'follower_count':follower_count,'following_count':following_count})

# def user_page(request):
#     return render(request, 'template-app/user-page.html')


@login_required(login_url='login')
def user_page(request):
    if request.user.is_authenticated:
        username = request.user
        profile = customUser.objects.filter(username=username).values()
        other_users_pins = Pin.objects.exclude(user_id=request.user.id)
        print(request.user.id)
        print(other_users_pins)
        return render(request, 'template-app/user-page.html', {'profile': profile,'pins':other_users_pins})
    return render(request, 'login.html') 



