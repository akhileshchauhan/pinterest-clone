function displayHeadings() {
    var headings = ["Get time snack idea", "Home Decor", "Outfit Idea", "DIY idea"];
    var index = 0;

    function displayNextHeading() {
        var Heading = headings[index];

        var newHead = document.getElementById("heading");
        
        if(index > 0){
            
            newHead.innerHTML = Heading;
            newHead.style.backgroundColor = 'orangered';
            newHead.style.color = '#fff'
        }
        else{
            newHead.style.backgroundColor = '#fff';
            newHead.style.color = 'orangered'
            newHead.innerHTML = Heading;
        }
        index = (index + 1) % headings.length;
    }

    setInterval(displayNextHeading, 2000);
}

displayHeadings()


var grid = document.querySelector('.pin-container');
var masonry = new Masonry(grid, {
    itemSelector: '.pin',
    columnWidth: '.pin',
    gutter: 16,
    fitWidth: true // Adjusts the container width based on the grid items
});




// document.addEventListener("DOMContentLoaded", function() {
//     const images = document.querySelectorAll(".pin");

//     images.forEach((image, index) => {
//         image.style.animationDelay = `${index * 0.5}s`; // Delay animation for each image
//         image.style.opacity = 1; // Make the image visible after the delay
//     });
// });