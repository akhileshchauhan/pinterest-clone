
document.addEventListener('DOMContentLoaded', function () {
    console.log('Hiiii');

    function fetchFollowers(username) {
        fetch(`/followers/${username}/`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                console.log(response);
                return response.json();
            })
            .then(data => {

                const followersList = document.getElementById('followersList');
                followersList.innerHTML = '';
                data.followers.forEach(follower => {

                    const followerItem = document.createElement('div');
                    followerItem.classList.add('follow-list');
                    var buttonAnchor = document.createElement('a');
                    buttonAnchor.href = `/dashboard/${follower}`;
                    buttonAnchor.classList.add('logo');


                    var capitalLetterDiv = document.createElement('div');
                    capitalLetterDiv.classList.add('capital-letter');
                    capitalLetterDiv.textContent = follower.charAt(0).toUpperCase();


                    buttonAnchor.appendChild(capitalLetterDiv);


                    var usernameDiv = document.createElement('div');
                    usernameDiv.classList.add('follow-username');

                    var usernameHeading = document.createElement('h3');
                    usernameHeading.textContent = follower;


                    usernameDiv.appendChild(usernameHeading);
                    followerItem.appendChild(buttonAnchor);
                    followerItem.appendChild(usernameDiv);
                    followersList.appendChild(followerItem);
                });
                console.log(data.followers.length);
            })
            .catch(error => {
                console.error('Fetch error:', error);
            });
    }

    function fetchFollowing(username) {
        fetch(`/following/${username}/`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                const followersList = document.getElementById('followingList');
                followersList.innerHTML = '';
                data.following.forEach(follower => {
                    console.log(follower)
                    const followerItem = document.createElement('div');
                    followerItem.classList.add('follow-list');
                    var buttonAnchor = document.createElement('a');
                    buttonAnchor.href = `/dashboard/${follower}`;
                    buttonAnchor.classList.add('logo');

                    // Create the capital-letter div
                    var capitalLetterDiv = document.createElement('div');
                    capitalLetterDiv.classList.add('capital-letter');
                    capitalLetterDiv.textContent = follower.charAt(0).toUpperCase();

                    buttonAnchor.appendChild(capitalLetterDiv);

                    var usernameDiv = document.createElement('div');
                    usernameDiv.classList.add('follow-username');

                    var usernameHeading = document.createElement('h3');
                    usernameHeading.textContent = follower;

                    usernameDiv.appendChild(usernameHeading);

                    followerItem.appendChild(buttonAnchor);
                    followerItem.appendChild(usernameDiv);
                    followersList.appendChild(followerItem);

                });
            })
            .catch(error => {
                console.error('Fetch error:', error);

            });
    }


    document.getElementById('openFollowersModal').addEventListener('click', function () {
        const username = this.dataset.username;
        console.log('Hii')
        fetchFollowers(username);
        document.getElementById('followersModal').style.display = 'block';
    });


    document.getElementById('openFollowingModal').addEventListener('click', function () {
        console.log('Hii')
        const username = this.dataset.username;
        fetchFollowing(username);
        document.getElementById('followingModal').style.display = 'block';
    });


    document.querySelectorAll('.close').forEach(closeButton => {
        closeButton.addEventListener('click', function () {
            closeButton.parentElement.parentElement.style.display = 'none';
        });
    });
});






// document.addEventListener('DOMContentLoaded', function () {
//     document.querySelectorAll('.follow').forEach(div => {
//         const username = div.dataset.username;
//         const followKey = `followState_${username}`;

//         let isFollowing = localStorage.getItem(followKey);
//         console.log(isFollowing)

//         if (isFollowing === null) {
//             isFollowing = 'false';
//         }


//         if (isFollowing === 'true') {
//             div.innerText = 'Unfollow';
//             // console.log(isFollowing)
//         }


//         div.addEventListener('click', () => {
//             const imageId = div.dataset.imageId;
//             const isCheck = !(isFollowing === 'true');

//             console.log('Div clicked:', username, imageId, isCheck, isFollowing);


//             fetch(`/follow/${username}/`, {
//                 method: isCheck ? 'POST' : 'DELETE',
//                 headers: {
//                     'X-CSRFToken': getCookie('csrftoken'),
//                     'Content-Type': 'application/json'
//                 },
//                 credentials: 'same-origin'
//             })
//                 .then(response => {
//                     console.log(response)
//                     if (!response.ok) {
//                         throw new Error('Network response was not ok');
//                     }
//                     return response.json();
//                 })
//                 .then(data => {
//                     if (data.success) {
//                         localStorage.setItem(followKey, isCheck);
//                         div.innerText = isCheck ? 'Unfollow' : 'Follow';
//                         this.location.reload();
//                     } else if (data.error) {
//                         console.log(data.error);
//                     }
//                 })
//                 .catch(error => {
//                     console.error('Fetch error:', error);
//                     if (error.message === 'Network response was not ok') {
//                         console.log(error.message)
//                     }
//                 });
//         });
//     });
// });


document.addEventListener('DOMContentLoaded', function () {
    document.querySelectorAll('.follow').forEach(div => {
        const username = div.dataset.username;
        const followKey = `followState_${username}`;

        let isFollowing = localStorage.getItem(followKey);
        
        // Initialize isFollowing to 'false' if it's not set
        if (isFollowing === null) {
            isFollowing = 'false';
            localStorage.setItem(followKey, isFollowing);
        }

        // Update button text based on isFollowing state
        div.innerText = isFollowing === 'true' ? 'Unfollow' : 'Follow';

        div.addEventListener('click', () => {
            const isCheck = isFollowing !== 'true'; // Toggle following state

            fetch(`/follow/${username}/`, {
                method: isCheck ? 'POST' : 'DELETE',
                headers: {
                    'X-CSRFToken': getCookie('csrftoken'),
                    'Content-Type': 'application/json'
                },
                credentials: 'same-origin'
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(data => {
                    if (data.success) {
                        isFollowing = isCheck ? 'true' : 'false'; // Update isFollowing state
                        localStorage.setItem(followKey, isFollowing);
                        div.innerText = isCheck ? 'Unfollow' : 'Follow'; // Update button text
                    } else if (data.error) {
                        console.log(data.error);
                    }
                })
                .catch(error => {
                    console.error('Fetch error:', error);
                });
        });
    });
});





function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}




window.addEventListener('load', function () {
    var container = document.querySelector('.pin-img1');
    var img = container.querySelector('img');
    var aspectRatio = 3 / 5; // Example aspect ratio
    console.log('img');
    img.style.height = container.offsetWidth / aspectRatio + 'px';
});




document.addEventListener('DOMContentLoaded', function () {
    const commentsLink = document.getElementById('comments-link');
    const commentsSection = document.getElementById('comments-section');

    commentsLink.addEventListener('click', function (event) {
        event.preventDefault();
        commentsSection.style.display = 'block'
        const pinId = commentsLink.getAttribute('data-pin-id');
        fetchComments(pinId);
    });

    function fetchComments(pinId) {
        fetch(`/comment_data/${pinId}/`)
            .then(response => response.json())
            .then(data => {
                const comments = JSON.parse(data);
                commentsSection.innerHTML = '';
                const commentElements = renderComments(comments);
                commentElements.forEach(commentElement => {
                    commentsSection.appendChild(commentElement);
                });
            })
            .catch(error => console.error('Error fetching comments:', error));
    }

    function renderComments(comments, parentId = null) {
        return comments.filter(comment => comment.fields.parent_comment === parentId).map(comment => {
            const commentItem = document.createElement('div');
            commentItem.classList.add('comment-item');
            commentItem.innerHTML = `
                <p><strong>${comment.fields.author}</strong> - ${new Date(comment.fields.created_at).toLocaleString()}</p>
                <p>${comment.fields.text}</p>
                <button class="reply-button" data-comment-id="${comment.pk}">Reply</button>
                <div class="reply-form-container" id="reply-form-container-${comment.pk}" style="display: none;">
                    <form method="post" class="reply-form" action="/comment/${comment.fields.pin}/">
                        <input type="hidden" name="csrfmiddlewaretoken" value="${getCookie('csrftoken')}">
                        <textarea name="comment-text" rows="2" cols="50"></textarea>
                        <input type="hidden" name="parent_id" value="${comment.pk}">
                        <input type="submit" value="Reply">
                    </form>
                </div>
            `;

            const replies = renderComments(comments, comment.pk);
            replies.forEach(reply => commentItem.appendChild(reply));

            const replyButton = commentItem.querySelector(`.reply-button[data-comment-id="${comment.pk}"]`);
            replyButton.addEventListener('click', function () {
                const replyFormContainer = commentItem.querySelector(`#reply-form-container-${comment.pk}`);
                replyFormContainer.style.display = replyFormContainer.style.display === 'none' ? 'block' : 'none';
            });

            return commentItem;
        });
    }

})